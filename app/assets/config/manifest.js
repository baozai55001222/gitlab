//= link_tree ../images

//= link application_utilities.css
//= link application_utilities_dark.css
//= link application.css
//= link application_dark.css

//= link_directory ../stylesheets/startup .css

//= link print.css
//= link mailer.css
//= link mailer_client_specific.css
//= link notify.css
//= link notify_enhanced.css

//= link mailers/highlighted_diff_email.css
//= link page_bundles/_mixins_and_variables_and_functions.css

//= link page_bundles/admin/application_settings_metrics_and_profiling.css
//= link page_bundles/admin/geo_nodes.css
//= link page_bundles/admin/geo_replicable.css
//= link page_bundles/admin/jobs_index.css

//= link page_bundles/alert_management_details.css
//= link page_bundles/alert_management_settings.css
//= link page_bundles/boards.css
//= link page_bundles/branches.css
//= link page_bundles/build.css
//= link page_bundles/ci_status.css
//= link page_bundles/cluster_agents.css
//= link page_bundles/clusters.css
//= link page_bundles/dashboard.css
//= link page_bundles/dashboard_projects.css
//= link page_bundles/design_management.css
//= link page_bundles/editor.css
//= link page_bundles/environments.css
//= link page_bundles/error_tracking_details.css
//= link page_bundles/error_tracking_index.css
//= link page_bundles/escalation_policies.css
//= link page_bundles/graph_charts.css
//= link page_bundles/group.css
//= link page_bundles/ide.css
//= link page_bundles/import.css
//= link page_bundles/incident_management_list.css
//= link page_bundles/incidents.css
//= link page_bundles/issuable.css
//= link page_bundles/issuable_list.css
//= link page_bundles/issues_list.css
//= link page_bundles/issues_show.css
//= link page_bundles/jira_connect.css
//= link page_bundles/jira_connect_users.css
//= link page_bundles/learn_gitlab.css
//= link page_bundles/members.css
//= link page_bundles/merge_conflicts.css
//= link page_bundles/merge_requests.css
//= link page_bundles/milestone.css
//= link page_bundles/new_namespace.css
//= link page_bundles/notifications.css
//= link page_bundles/oncall_schedules.css
//= link page_bundles/operations.css
//= link page_bundles/pipeline.css
//= link page_bundles/pipeline_editor.css
//= link page_bundles/pipeline_schedules.css
//= link page_bundles/pipelines.css
//= link page_bundles/profile.css
//= link page_bundles/profile_two_factor_auth.css
//= link page_bundles/profiles/preferences.css
//= link page_bundles/project.css
//= link page_bundles/projects_edit.css
//= link page_bundles/prometheus.css
//= link page_bundles/releases.css
//= link page_bundles/reports.css
//= link page_bundles/runner_details.css
//= link page_bundles/search.css
//= link page_bundles/settings.css
//= link page_bundles/signup.css
//= link page_bundles/terminal.css
//= link page_bundles/terms.css
//= link page_bundles/todos.css
//= link page_bundles/tree.css
//= link page_bundles/users.css
//= link page_bundles/wiki.css
//= link page_bundles/work_items.css
//= link page_bundles/xterm.css

//= link lazy_bundles/cropper.css
//= link lazy_bundles/select2.css
//= link lazy_bundles/gridstack.css

//= link performance_bar.css
//= link disable_animations.css
//= link test_environment.css
//= link snippets.css
//= link fonts.css

//= link_tree ../javascripts/locale .js
//= link gettext/all

//= link emoji_sprites.css
//= link errors.css

//= link_directory ../stylesheets/themes .css

//= link_directory ../stylesheets/highlight/themes .css
//= link highlight/diff_custom_colors_addition.css
//= link highlight/diff_custom_colors_deletion.css

// @gitlab/fonts package
//= link gitlab-sans/GitLabSans.woff2
//= link jetbrains-mono/JetBrainsMono.woff2

// @gitlab-svg package
//= link icons.svg
//= link icons.json
//= link_tree ../../../node_modules/@gitlab/svgs/dist/illustrations .svg
//= link_tree ../../../node_modules/@gitlab/svgs/dist/illustrations .png

//= link xterm/src/xterm.css

//= link snowplow/sp.js
